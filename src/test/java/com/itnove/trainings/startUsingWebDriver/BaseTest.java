package com.itnove.trainings.startUsingWebDriver;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

public class BaseTest {
    public RemoteWebDriver driver;
    public Actions hover;
    public static long timeOut = 10;
    public static LocalRemoteWebDriverWait wait;
    public static JavascriptExecutor jse;

    @BeforeMethod
    public void setUp() throws MalformedURLException {
        String browser = System.getProperty("browser");
        if (browser.equalsIgnoreCase("chrome")) {
            DesiredCapabilities capabilities = DesiredCapabilities.chrome();
            System.setProperty("webdriver.chrome.driver", "src" + File.separator + "main" + File.separator + "resources" + File.separator + "chromedriver-macos");
            driver = new ChromeDriver(capabilities);
        } else {
            DesiredCapabilities capabilities = DesiredCapabilities.firefox();
            System.setProperty("webdriver.gecko.driver",
                    "src" + File.separator + "main"
                            + File.separator + "resources"
                            + File.separator + "geckodriver-macos");
            driver = new FirefoxDriver(capabilities);
        }

//        DesiredCapabilities caps = DesiredCapabilities.edge();
//        caps.setCapability("platform", "Windows 10");
//        caps.setCapability("version", "16.16299");
//        driver = new RemoteWebDriver(
//                new URL("http://itnove:4394d787-3244-4c03-9490-3816f2bb683b@ondemand.saucelabs.com:80/wd/hub"),caps);
        wait = new LocalRemoteWebDriverWait(driver, timeOut);
        hover = new Actions(driver);
        driver.manage().deleteAllCookies();
        driver.manage().timeouts().pageLoadTimeout(timeOut, TimeUnit.SECONDS);
        driver.manage().timeouts().setScriptTimeout(timeOut, TimeUnit.SECONDS);
        driver.manage().timeouts().implicitlyWait(timeOut, TimeUnit.SECONDS);
    }

    @AfterMethod
    public void tearDown() {
        driver.quit();
    }

}
