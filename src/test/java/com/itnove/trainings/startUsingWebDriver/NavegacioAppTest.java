package com.itnove.trainings.startUsingWebDriver;

import org.openqa.selenium.By;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class NavegacioAppTest extends BaseTest {
    @Test
    public void testNavegacioApp() throws Exception {
        driver.get("http://localhost:3000");
        driver.findElement(By.linkText("Courses")).click();
        driver.findElement(By.linkText("view course")).click();
        driver.findElement(By.linkText("Start course")).click();
        assertEquals(driver.getTitle(),
                "Hello Contentful | APIs — The Example App");
    }
}
